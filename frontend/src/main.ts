import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './app/app.module';

import * as moment from 'moment-timezone';

moment.tz.setDefault('Europe/Moscow');
moment.locale('ru');

enableProdMode();

platformBrowserDynamic().bootstrapModule(AppModule);
