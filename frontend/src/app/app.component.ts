import {Component, Input} from '@angular/core';
import {IMenu} from "./modules/menu/menu-item/menu-item.interface";
import {MainComponent} from "./pages/main/main-page.component";
import {CatalogComponent} from "./pages/catalog/catalog.component";
import {CorporateComponent} from "app/pages/corporate/corporate.component";
import {ShippingAndPaymentComponent} from "./pages/shipping-and-payment/shipping-and-payment.component";
import {FaqComponent} from "./pages/faq/faq.component";
import {ContactsComponent} from "./pages/contacts/contacts.component";
import {InstagramComponent} from "./pages/instagram/instagram.component";
import {NavigationEnd, Router} from "@angular/router";
import {GoogleAnalyticsEventsService} from "./google-analytics-events.service";

@Component({
    moduleId: module.id,
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent {
    @Input() public menu: IMenu[] = [
       /* {
            name: 'Главная', url: MainComponent.PATH
        },*/
        {
            name: 'Каталог подарков', url: CatalogComponent.PATH
        },
      /*  {
            name: 'Корпоративные подарки', url: CorporateComponent.PATH
        },*/
        {
            name: 'Доставка и оплата', url: ShippingAndPaymentComponent.PATH
        },
        {
            name: 'Faq', url: FaqComponent.PATH
        },
        {
            name: 'Контакты', url: ContactsComponent.PATH
        },
       /* {
            name: 'Instagram', url: InstagramComponent.PATH
        }*/
    ];
    constructor(public router: Router, public googleAnalyticsEventsService: GoogleAnalyticsEventsService) {
        this.router.events.subscribe(event => {
             if (event instanceof NavigationEnd) {
                ga('set', 'page', event.urlAfterRedirects);
                ga('send', 'pageview');
             }
        });
    }
}