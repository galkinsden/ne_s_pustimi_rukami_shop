import {ModuleWithProviders, NgModule} from "@angular/core";
import {FaqComponent} from "./faq.component";
import {MatTabsModule} from "@angular/material";


@NgModule({
    declarations: [FaqComponent],
    exports: [FaqComponent],
    imports: [MatTabsModule],
    providers: [],
    entryComponents: []
})
export class FaqModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: FaqModule};
    }
}