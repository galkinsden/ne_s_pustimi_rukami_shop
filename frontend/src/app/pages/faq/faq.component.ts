﻿import {Component, ViewEncapsulation} from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: 'faq.component.html',
    styleUrls: ['./faq.component.scss'],
    encapsulation: ViewEncapsulation.None,
})

export class FaqComponent {

    public static PATH: string = 'faq';

    constructor() {}
}
