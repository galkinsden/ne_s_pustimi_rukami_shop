﻿import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: 'contacts.component.html',
    styleUrls: ['./contacts.component.scss']
})

export class ContactsComponent {

    public static PATH: string = 'contacts';

    constructor() {}
}
