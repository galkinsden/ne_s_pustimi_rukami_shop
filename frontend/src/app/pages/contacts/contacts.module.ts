import {ModuleWithProviders, NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ContactsComponent} from "./contacts.component";
import {MatIconModule} from "@angular/material";
import {SvgIconModule} from "../../modules/svg-icon/svg-icon.module";

@NgModule({
    declarations: [
          ContactsComponent
    ],
    exports: [
        ContactsComponent
    ],
    imports: [
        CommonModule,
        MatIconModule,
        SvgIconModule
    ],
    providers: [

    ],
    entryComponents: []
})
export class ContactsModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: ContactsModule};
    }
}