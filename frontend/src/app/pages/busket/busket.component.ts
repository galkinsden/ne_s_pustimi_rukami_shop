﻿import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: 'busket.component.html',
    styleUrls: ['./busket.component.scss']
})
export class BusketComponent {
    public static PATH: string = 'busket';

    public constructor() {

    }
}