﻿﻿import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: 'main-page.component.html',
    styleUrls: ['./main-page.component.scss']
})

export class MainComponent {
    public static PATH: string = 'main';

    public constructor() {

    }
}