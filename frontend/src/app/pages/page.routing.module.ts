import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainComponent} from './main/main-page.component';
import {CatalogComponent} from './catalog/catalog.component';
import {CorporateComponent} from './corporate/corporate.component';
import {ShippingAndPaymentComponent} from './shipping-and-payment/shipping-and-payment.component';
import {FaqComponent} from './faq/faq.component';
import {ContactsComponent} from './contacts/contacts.component';
import {InstagramComponent} from './instagram/instagram.component';
import {BusketComponent} from './busket/busket.component';

const routes: Routes = [
   /* { path: MainComponent.PATH, component: MainComponent },*/
    { path: CatalogComponent.PATH, component: CatalogComponent },
  /*  { path: CorporateComponent.PATH, component: CorporateComponent },*/
    { path: ShippingAndPaymentComponent.PATH, component: ShippingAndPaymentComponent },
    { path: FaqComponent.PATH, component: FaqComponent },
    { path: ContactsComponent.PATH, component: ContactsComponent },
 /*   { path: InstagramComponent.PATH, component: InstagramComponent },*/
    { path: BusketComponent.PATH, component: BusketComponent },
    // Остальное на каталог
    { path: '**', redirectTo: CatalogComponent.PATH }
];

/**
 * Роутинг приложения
 */
@NgModule({
		imports: [RouterModule.forRoot(routes)],
		providers: [],
		exports: [RouterModule]
	})
export class PageRoutingModule {}
