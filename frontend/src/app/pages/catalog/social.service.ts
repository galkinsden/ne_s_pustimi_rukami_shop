import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {SocialConnector} from "../../connectors/social.connector";
import {ISocialFullInfo, SocialFullInfoModel} from "./models/social-full-Info.model";
import {Observable} from "rxjs/Observable";

@Injectable()
export class SocialService {
    private socialConnector: SocialConnector;
    public static LSLikeCollectionName = 'likedItems';

    public constructor(protected http: HttpClient) {
        this.socialConnector = new SocialConnector(http);
    }

    public isLiked(id: string): boolean {
        try {
            let likedItems: Object = this.getLikesFromLS();
            if (likedItems) {
                return !!likedItems[id];
            } else {
                localStorage.setItem('likedItems', JSON.stringify({}));
                return false;
            }
        } catch (e) {
            return false;
        }

    }

    public getDataForAll(): Observable<SocialFullInfoModel[]> {
        return this.socialConnector.getAllInfo()
            .map((result: ISocialFullInfo[]) => result.map(elem => new SocialFullInfoModel(elem)))
    }

    public getLikesById(id: string): Observable<SocialFullInfoModel[]> {
        return this.socialConnector.getLikesById(id)
            .map((result: ISocialFullInfo[]) => result.map(elem => new SocialFullInfoModel(elem)))
    }

    public getCommentsById(id: string): Observable<SocialFullInfoModel[]> {
        return this.socialConnector.getCommentsById(id)
            .map((result: ISocialFullInfo[]) => result.map(elem => new SocialFullInfoModel(elem)))
    }

    public getPurchasesById(id: string): Observable<SocialFullInfoModel[]> {
        return this.socialConnector.getPurchasesById(id)
            .map((result: ISocialFullInfo[]) => result.map(elem => new SocialFullInfoModel(elem)))
    }

    public getViewById(id: string): Observable<SocialFullInfoModel[]> {
        return this.socialConnector.getViewById(id)
            .map((result: ISocialFullInfo[]) => result.map(elem => new SocialFullInfoModel(elem)))
    }

    public setCommentById(id: string, text: string): Observable<boolean> {
        return this.socialConnector.setCommentById(id, text)
            .map((result: boolean) => result)
    }

    public setLikeById(id: string): Observable<boolean> {
        return this.socialConnector.setLikeById(id)
            .map((result: boolean) => {
                this.addLikedToLS(id);
                return result;
            })
    }

    public setPurchasesById(id: string, count?: number): Observable<boolean> {
        return this.socialConnector.setPurchasesById(id, count)
            .map((result: boolean) => result)
    }

    public setViewById(id: string): Observable<boolean> {
        return this.socialConnector.setViewById(id)
            .map((result: boolean) => result)
    }

    private addLikedToLS(id: string) {
        if (!this.isLiked(id)) {
            this.setLikeToLS(id);
        }
    }

    private getLikesFromLS(): Object {
        return localStorage.getItem(SocialService.LSLikeCollectionName) && JSON.parse(localStorage.getItem(SocialService.LSLikeCollectionName))
    }

    private setLikeToLS(id: string) {
        let refreshedCollection = this.getLikesFromLS();
        refreshedCollection[id] = true;
        localStorage.setItem(SocialService.LSLikeCollectionName, JSON.stringify(refreshedCollection));
    }
}