﻿import {ICheckboxMeta} from '../../../modules/array-checkboxes/array-checkboxes.component';
import {
    Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges,
    ViewEncapsulation
} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {
    CatalogControllerService,
    IFilterParams
} from 'app/pages/catalog/catalog-controller/catalog-conroller.service';
import {EStateModel} from "../../../modules/state/state-model.enum";

export enum EGender {
    MALE = 'MALE',
    FEMALE = 'FEMALE',
}

export interface ISorter {
    code: ESorter;
    name: string;
}

export interface IGender {
    code: EGender;
    name: string;
}

export enum ESorter {
    NUM = 'NUM',
    NEWEST = 'NEWEST',
    UP = 'UP',
    DOWN = 'DOWN'
}

export enum EPackage {
    WOOD = 'WOOD',
    CARBOARD = 'CARBOARD',
    PACKET = 'PACKET'
}

export enum ECollections {
    NEW_YEAR = 'NEW_YEAR',
    CHRISTMAS = 'CHRISTMAS',
    ST_VALENTINE_DAY = 'ST_VALENTINE_DAY',
    HAPPY_BIRTHDAY = 'HAPPY_BIRTHDAY',
    MARCH_EIGHT = 'MARCH_EIGHT',
    TWENTY_THREE_FEBRUARY = 'TWENTY_THREE_FEBRUARY',
    CORPORATE = 'CORPORATE',
}

const COLLECTIONS: Array<ICheckboxMeta<string>> = [
    {
        type: ECollections.NEW_YEAR,
        label: 'Новый год',
        key: '1',
        checked: false
    },
    {
        type: ECollections.HAPPY_BIRTHDAY,
        label: 'День рождения',
        key: '2',
        checked: false
    },
    {
        type: ECollections.MARCH_EIGHT,
        label: '8 марта',
        key: '3',
        checked: false
    },
    {
        type: ECollections.TWENTY_THREE_FEBRUARY,
        label: '23 февраля',
        key: '4',
        checked: false
    },
    {
        type: ECollections.CHRISTMAS,
        label: 'Рождество',
        key: '5',
        checked: false
    },
    {
        type: ECollections.ST_VALENTINE_DAY,
        label: 'День Святого Валентина',
        key: '6',
        checked: false
    },
    {
        type: ECollections.CORPORATE,
        label: 'Корпоративные подарки',
        key: '7',
        checked: false
    },
];

const SORTER: ISorter[] = [
    {code: ESorter.NUM, name: 'По номеру'},
    {code: ESorter.NEWEST, name: 'Новейшие'},
    {code: ESorter.UP, name: 'Цена (низкая -высокая)'},
    {code: ESorter.DOWN, name: 'Цена (высокая - низкая)'},
];

export const PACKAGES: Array<ICheckboxMeta<EPackage>> = [
    {
        type:  EPackage.WOOD,
        label: 'Деревянная коробка',
        key: '1',
        checked: false
    },
    {
        type:  EPackage.CARBOARD,
        label: 'Крафт-коробка',
        key: '2',
        checked: false
    },
    {
        type:  EPackage.PACKET,
        label: 'Пакет',
        key: '3',
        checked: false
    }
];

export const GENDER: Array<ICheckboxMeta<EGender>> = [
    {
        type:  EGender.FEMALE,
        label: 'Женские',
        key: '1',
        checked: false
    },
    {
        type:  EGender.MALE,
        label: 'Мужские',
        key: '2',
        checked: false
    }
];

@Component({
    moduleId: module.id,
    selector: 'catalog-filter',
    templateUrl: 'catalog-controller.component.html',
    styleUrls: ['./catalog-controller.component.scss'],
    encapsulation : ViewEncapsulation.None,
})

export class CatalogControllerComponent implements OnChanges, OnInit {

    @Output() public filterFormSubmit: EventEmitter<IFilterParams> = new EventEmitter<IFilterParams>();
    @Input() public minSlider: number = 0;
    @Input() public maxSlider: number = 0;
    @Input() public state: EStateModel = EStateModel.NONE;
    public isMobile: boolean = false;

    public filterForm: FormGroup;

    public collections: Array<ICheckboxMeta<string>> = COLLECTIONS;

    public stepSlider: number = 10;

    public sorter: ISorter[] = SORTER;

    public genders: Array<ICheckboxMeta<EGender>> = GENDER;

    public packages: Array<ICheckboxMeta<EPackage>> = PACKAGES;

    public constructor(
        private catalogControllerService: CatalogControllerService
    ) {

    }

    private initFilterForm(): void {
        this.filterForm = new FormGroup({
            collections: new FormControl([]),
            slider: new FormControl([]),
            sorter: new FormControl(null),
            genders: new FormControl([]),
            packages: new FormControl([])
        });
    }

    /**
     * заполнение формы фильтром из сервиса
     */
    private initFormValue(): void {
        this.filterForm.setValue(this.catalogControllerService.getFilter());
    }

    public ngOnChanges(changes: SimpleChanges): void {
        this.catalogControllerService.initFilter(COLLECTIONS, [this.minSlider, this.maxSlider]);
        this.initFilterForm();
        this.initFormValue();
    }

    ngOnInit() {
        this.isMobile = this.checkIsMobileDevice();
    }

    private checkIsMobileDevice() {
        return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
    };

    /**
     * Применить фильтр
     */
    public onFilterSubmit(): void {
        this.catalogControllerService.setFilter(this.filterForm.value);
        this.filterFormSubmit.emit(this.catalogControllerService.getFilter());
    }
}
