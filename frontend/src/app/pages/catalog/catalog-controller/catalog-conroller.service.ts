import {Injectable} from "@angular/core";
import {ECollections, EGender, EPackage, ISorter} from "./catalog-controller.component";
import {ICheckboxMeta} from "app/modules/array-checkboxes/array-checkboxes.component";

export interface IFilterParams {
    collections: Array<ICheckboxMeta<ECollections>>;
    slider: Array<number>;
    sorter: ISorter;
    genders: Array<ICheckboxMeta<EGender>>;
    packages: Array<ICheckboxMeta<EPackage>>;
}

@Injectable()
export  class  CatalogControllerService {
    public readonly initialFilter: IFilterParams =  {
        collections: [],
        slider: [],
        sorter: null,
        genders: [],
        packages: []
    };

    private _filter: IFilterParams = this.initialFilter;

    /**
     * Задает фильтр выбранный пользователем
     * @param {IFilterParams} filterSrc
     */
    public setFilter (filterSrc: IFilterParams): void {
        this._filter = filterSrc;
    }

    /**
     * Возвращает фильтр с выбранными параметрами
     * @returns {IFilterParams}
     */
    public getFilter(): IFilterParams {
        return this._filter;
    }

    /**
     * инициализирует выпадающие списки фильтра. их состав заранее не известен
     */
    public initFilter(collections, slider, sorter = null, genders = [], packages = []): void {
        this.initialFilter.collections = collections;
        this.initialFilter.slider = slider;
        this.initialFilter.sorter = sorter;
        this.initialFilter.genders = genders;
        this.initialFilter.packages = packages;
    }

}
