import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {SocialConnector} from "../../connectors/social.connector";
import {Observable} from "rxjs/Observable";
import {StatisticConnector} from "../../connectors/statistic.connector";
import {ISocialFullInfo, SocialFullInfoModel} from "./models/social-full-Info.model";
import {IStatisticInfo, StatisticInfoModel} from "./models/statistic-info.model";

@Injectable()
export class StatisticService {
    private statisticConnector: StatisticConnector;

    public constructor(protected http: HttpClient) {
        this.statisticConnector = new StatisticConnector(http);
    }

    public getStatistic(): Observable<StatisticInfoModel> {
        return this.statisticConnector.getStatistic()
            .map((result: IStatisticInfo) => new StatisticInfoModel(result))
    }


}