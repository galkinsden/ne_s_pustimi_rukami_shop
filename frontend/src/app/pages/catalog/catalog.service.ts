import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {CatalogModel, ICatalog} from "./models/catalog.model";
import {HttpClient} from "@angular/common/http";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {CatalogConnector} from "../../connectors/catalog.connector";
import {IAttachmentModel} from "./models/gift-with-attachments.model";
import {IGift} from "./models/gift.model";

@Injectable()
export class CatalogService {


    private catalogConnector: CatalogConnector;

    public constructor(
        protected http: HttpClient
    ) {
        this.catalogConnector = new CatalogConnector(http);
    }

    /**
     * Получаем список товаров
     * @returns {Observable<CatalogModel>}
     */
    public getAll(): Observable<CatalogModel> {
        return this.catalogConnector.getAll()
            .map((res: ICatalog) => new CatalogModel(res));
    }

    public getGiftById(id: string): Observable<IGift> {
        return this.catalogConnector.getGiftById(id)
            .map((res: IGift) => res);
    }

    public getGiftsByIds(ids: string[]): Observable<IGift[]> {
        return this.catalogConnector.getGiftsByIds(ids)
            .map((res: IGift[]) => res);
    }

   /* public getCatalogWithAttachmentsById(id: string): Observable<IGiftWithAttachments> {
        return this.catalogConnector.getCatalogWithAttachmentsById(id)
            .map((res: IGiftWithAttachments) => new GiftWithAttachmentsModel(res));
    }*/

    public getAttachmentsById(id: string): Observable<IAttachmentModel> {
        return this.catalogConnector.getAttachmentsById(id)
            .map((res: IAttachmentModel) => res);
    }

}