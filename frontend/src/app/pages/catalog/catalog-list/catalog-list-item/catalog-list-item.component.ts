﻿import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {GiftModel} from '../../models/gift.model';
import {MatDialog} from '@angular/material';
import {CatalogListItemModalComponent} from './catalog-list-item-modal/catalog-list-item-modal.component';
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs/Subscription";
import {GoogleAnalyticsEventsService} from "../../../../google-analytics-events.service";
import {SocialService} from "../../social.service";
import {EModes, SocialFullInfoModel} from "../../models/social-full-Info.model";
import {debug} from "util";
import {UnSnackBarService} from "../../../../modules/snackbar/un-snack-bar.service";

export interface ICatalogListItemModal {
    item: GiftModel;
}

@Component({
    moduleId: module.id,
    selector: 'catalog-list-item',
    templateUrl: 'catalog-list-item.component.html',
    styleUrls: ['./catalog-list-item.component.scss'],
})

export class CatalogListItemComponent implements OnInit, OnDestroy {

    @Input() public item: GiftModel;
    @Input() public modalGiftId: string;
    public open: boolean = false;
    public openSubscribtion: Subscription = null;

    public constructor(protected dialog: MatDialog,
                       protected router: Router,
                       protected route: ActivatedRoute,
                       protected googleAnalyticsEventsService: GoogleAnalyticsEventsService,
                       protected socialService: SocialService,
                       protected snackBarService: UnSnackBarService) {

    }

    public ngOnInit(): void {
        if (this.modalGiftId && this.item.id && this.modalGiftId === this.item.id) {
            this.openDialog();
        }

        this.isLiked() && this.changePriceOnLike();
    }

    public ngOnDestroy(): void {
        this.openSubscribtion && this.openSubscribtion.unsubscribe();
    }

    public getRibbonName(mode: EModes): string {
        switch (mode) {
            case(EModes.hit):
                return 'Хит';
            case (EModes.sale):
                return 'Скидка';
            case(EModes.empty):
                return 'Нет в наличии';
            default:
                return '';
        }
    }
    public getRibbonClass(mode: EModes): Object {
        let tmp = {};
        tmp[mode] = true;
        return tmp;
    }

    public setLike(e: MouseEvent) {
        event.stopPropagation();
        let subscribe = this.socialService.setLikeById(this.item.id).subscribe(() => {
            this.item.social.like++;
            if(Array.isArray(this.item.social.modes)) {
                this.item.social.modes.push(EModes.sale);
            } else {
                this.item.social.modes = [EModes.sale];
            }
            this.snackBarService.openLike(`Спасибо за ваш лайк, мы сделали Вам комплимент! Цена на "${this.item.name}" приятно изменена.`);
            this.changePriceOnLike();
            subscribe.unsubscribe();
        });
    }

    public isLiked(): boolean {
        return this.socialService.isLiked(this.item.id);
    }

    public openDialog(): void {
        let subscription = this.socialService.setViewById(this.item.id).subscribe(() => {
            subscription.unsubscribe();
            this.item.social.view++;
        });
        this.googleAnalyticsEventsService.emitEvent("Catalog", `clickFullInfo`, `${this.item.name}`, 1);
        this.router.navigate(['./'], {
            relativeTo: this.route,
            replaceUrl: true,
            queryParams: {
                id: this.item.id
            }
        });
        this.openSubscribtion = this.dialog.open(CatalogListItemModalComponent, {
            data: {
                item: this.item
            }
        })
            .afterClosed()
            .subscribe(() => {
                this.router.navigate(['./'], {
                    relativeTo: this.route,
                    replaceUrl: true,
                    queryParams: {}
                });
            });
    }

    public onEnter(): void {
        this.open = true;
    }

    public onLeave(): void {
        this.open = false;
    }

    private changePriceOnLike() {
        this.item.priceOld = this.item.priceCurrent;
        this.item.priceCurrent = Math.round(this.item.priceCurrent * 0.99);
    }
}
