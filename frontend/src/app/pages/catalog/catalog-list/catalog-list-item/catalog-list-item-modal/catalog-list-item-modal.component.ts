import {Component, Inject, ViewEncapsulation, ElementRef, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {ICatalogListItemModal} from '../catalog-list-item.component';
import {ImgModel} from '../../../models/img.model';
import {BusketService, IPurchase} from '../../../../../modules/busket/busket.service';
import {FormControl} from '@angular/forms';
import {CatalogService} from "../../../catalog.service";
import {Observable} from "rxjs/Observable";
import {GiftWithAttachmentsModel, IAttachmentModel} from "../../../models/gift-with-attachments.model";
import {UnSnackBarService} from "../../../../../modules/snackbar/un-snack-bar.service";
import {StateModel} from "../../../../../modules/state/state.model";
import {EStateModel} from "../../../../../modules/state/state-model.enum";
import {GoogleAnalyticsEventsService} from "../../../../../google-analytics-events.service";

@Component({
    selector: 'catalog-list-item-modal',
    templateUrl: 'catalog-list-item-modal.component.html',
    styleUrls: ['./catalog-list-item-modal.component.scss']
})
export class CatalogListItemModalComponent extends StateModel implements OnInit {

    public currentImg: ImgModel;
    public $Model:  Observable<GiftWithAttachmentsModel>;
    public count: FormControl = new FormControl(1);

    public constructor(
        protected unSnackBarService: UnSnackBarService,
        protected ref: ElementRef,
        protected busketService: BusketService,
        protected catalogService: CatalogService,
        protected dialog: MatDialog,
        protected dialogRef: MatDialogRef<CatalogListItemModalComponent>,
        protected googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        @Inject(MAT_DIALOG_DATA) protected data: ICatalogListItemModal
    ) {
        super(EStateModel.LOADING);
    }

    public ngOnInit(): void {
        this.$Model = this.catalogService.getAttachmentsById(this.data.item.id)
            .map((element: IAttachmentModel) => {
                this.state = EStateModel.LOADED;
                return new GiftWithAttachmentsModel(this.data.item, element);
            })
       /*     .catch((error: any) => {
                this.state = EStateModel.ERROR;
                return new GiftWithAttachmentsModel(this.data.item, null);
            });*/

        // Добавляем классы в контейнер модалки
        if (this.ref.nativeElement && this.ref.nativeElement.parentNode && this.ref.nativeElement.parentNode.parentNode) {
            this.ref.nativeElement.parentNode.style.position = 'relative';
            this.ref.nativeElement.parentNode.parentNode.className += ' catalog-item-modal col-xs-12 col-sm-12 col-md-12 col-lg-12';
            this.ref.nativeElement.parentNode.parentNode.style.maxWidth = '100%';
        }
        this.currentImg = this.data.item.img.large;
    }

    public addToBusket(): void {
        const purchase: IPurchase = {
            id: this.data.item.id,
            count: this.count.value
        };
        this.googleAnalyticsEventsService.emitEvent("Busket", "clickAddToBusket", "Modal", 1);
        this.closeModal();
        this.busketService
            .setPurchase(purchase)
            .subscribe(
                () => {
                    this.unSnackBarService.openSucces(`${this.data.item.name} успешно добавлен в корзину!`);
                },
                () => {
                    this.unSnackBarService.openError(`Произошла ошибка при добавлении ${this.data.item.name}! Попробуйте позже или обратитесь в службу поддержки.`);
                });
    }

    public closeModal(): void {
        this.dialogRef.close();
    }
}
