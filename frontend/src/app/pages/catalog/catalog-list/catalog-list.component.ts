﻿import {ICheckboxMeta} from '../../../modules/array-checkboxes/array-checkboxes.component';

import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {CatalogService} from '../catalog.service';
import {CatalogModel} from '../models/catalog.model';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/last';
import {CatalogControllerService, IFilterParams} from 'app/pages/catalog/catalog-controller/catalog-conroller.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {GiftModel} from '../models/gift.model';
import {CatalogListModel} from '../models/catalog-list.model';
import {ECollections, EGender, EPackage, ESorter, GENDER} from '../catalog-controller/catalog-controller.component';
import {MatDialog} from '@angular/material';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {StateModel} from 'app/modules/state/state.model';
import {EStateModel} from "../../../modules/state/state-model.enum";
import * as moment from 'moment';
import {SocialConnector} from "../../../connectors/social.connector";
import {SocialService} from "../social.service";
import {EModes, SocialFullInfoModel} from "../models/social-full-Info.model";
import {UnSnackBarService} from "../../../modules/snackbar/un-snack-bar.service";

export interface ICatalogModelWithState {
    state: EStateModel;
    catalogModel: CatalogModel;
}

@Component({
    moduleId: module.id,
    selector: 'catalog-list',
    templateUrl: 'catalog-list.component.html',
    styleUrls: ['./catalog-list.component.scss']
})

export class CatalogListComponent extends StateModel implements OnInit {

    public model$: Observable<CatalogListModel>;
    private filterChange$: BehaviorSubject<IFilterParams> = new BehaviorSubject(null);

    public constructor(protected catalogService: CatalogService,
                       protected dialog: MatDialog,
                       protected router: Router,
                       protected route: ActivatedRoute,
                       protected socialService: SocialService) {
        super(EStateModel.LOADING);
    }

    public ngOnInit(): void {
        this.model$ = this.getList()
            .do(() => (this.state = EStateModel.LOADING))
            .map((catalogModel: CatalogModel) => {
                this.state = EStateModel.LOADED;
                return catalogModel;
            })
            .catch(() => {
                this.state = EStateModel.ERROR;
                return Observable.of(new CatalogModel(null));
            })
            .combineLatest(this.filterChange$, this.getSelectedIdNumberStream(), this.socialService.getDataForAll())
            .map(([catalogModel, filterParams, modalGiftId, socialsFullInfo]: [CatalogModel, IFilterParams, string, SocialFullInfoModel[]]) => {
                let list: GiftModel[] = catalogModel
                && catalogModel
                && catalogModel.list
                && catalogModel.list.length
                    ? catalogModel.list
                    : [];
                const [minSlider, maxSlider] = CatalogListComponent.getMinMax(list);
                if (list.length) {
                    if (filterParams) {
                        // Фильтруем по полу
                        if (filterParams.genders && filterParams.genders.length) {
                            list = list.filter((giftModel: GiftModel) => {
                                return filterParams.genders
                                    .map((g: ICheckboxMeta<EGender>) => g.type)
                                    .some((g: EGender) => giftModel.genders.includes(g));
                            });
                        }
                        // Фильтруем по упавке
                        if (filterParams.packages && filterParams.packages.length) {
                            list = list.filter((giftModel: GiftModel) => {
                                return filterParams.packages
                                    .map((packages: ICheckboxMeta<EPackage>) => packages.type)
                                    .some((c: EPackage) => giftModel.packages.includes(c));
                            });
                        }
                        // Фильтруем по типу подарка
                        if (filterParams.collections && filterParams.collections.length) {
                            list = list.filter((giftModel: GiftModel) => {
                                return filterParams.collections
                                    .map((collection: ICheckboxMeta<ECollections>) => collection.type)
                                    .some((c: ECollections) => giftModel.typeCollections.includes(c));
                            });
                        }
                        // Фильтруем по интервалу слайдера
                        if (filterParams.slider && filterParams.slider.length) {
                            list = list.filter((giftModel: GiftModel) =>
                                giftModel.priceCurrent >= filterParams.slider[0] && giftModel.priceCurrent <= filterParams.slider[1]);
                        }
                        // Сортируем по необходимому интервалу
                        if (filterParams.sorter && filterParams.sorter.code) {
                            switch (filterParams.sorter.code) {
                                case ESorter.UP:
                                    list = CatalogListComponent.getSortUp(list);
                                    break;
                                case ESorter.DOWN:
                                    list = CatalogListComponent.getSortDown(list);
                                    break;
                                case ESorter.NEWEST:
                                    list = CatalogListComponent.getSortByNewest(list);
                                    break;
                                case ESorter.NUM:
                                    list = CatalogListComponent.getSortByNum(list);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    list.forEach(element => {
                        let socialFullInfo: SocialFullInfoModel = socialsFullInfo.find(socialInfo => socialInfo.id == element.id);

                        if (!socialFullInfo) {
                            socialFullInfo = new SocialFullInfoModel();
                        }

                        if (!Array.isArray(socialFullInfo.modes)) {
                            socialFullInfo.modes = []
                        }

                        if (element.priceOld) {
                            !socialFullInfo.modes.includes(EModes.sale) && socialFullInfo.modes.push(EModes.sale)
                        }

                        if (element.disabled) {
                            !socialFullInfo.modes.includes(EModes.empty) && socialFullInfo.modes.push(EModes.empty)
                        }


                        element = new GiftModel(Object.assign(element, {social: socialFullInfo}));

                        return element;
                    })
                }


                return new CatalogListModel(list, minSlider, maxSlider, modalGiftId);
            })
            .map((catalogListModel: CatalogListModel) => {
                this.router.navigate(['./'], {
                    relativeTo: this.route,
                    replaceUrl: true,
                    queryParams: {
                        id: catalogListModel.modalGiftId
                    }
                });
                return catalogListModel;
            });
    }

    private getSelectedIdNumberStream(): Observable<string> {
        return this.route.queryParams
            .map((params: Params): string => {
                const result: string = params['id'];
                return result || null;
            });
    }

    private static getSortUp(list: GiftModel[]): GiftModel[] {
        return list.sort((a: GiftModel, b: GiftModel) => a.priceCurrent - b.priceCurrent);
    }

    private static getSortDown(list: GiftModel[]): GiftModel[] {
        return list.sort((a: GiftModel, b: GiftModel) => b.priceCurrent - a.priceCurrent);
    }

    private static getSortByNewest(list: GiftModel[]): GiftModel[] {
        return list.sort((a: GiftModel, b: GiftModel) => moment(b.date).diff(moment(a.date)));
    }

    private static getSortByNum(list: GiftModel[]): GiftModel[] {
        return list.sort((a: GiftModel, b: GiftModel) => a.num - b.num);
    }

    public static getMinMax(arr: GiftModel[]): [number, number] {
        if (arr && arr.length) {
            let min = arr[0].priceCurrent, max = arr[0].priceCurrent;
            for (let i = 1, len = arr.length; i < len; i++) {
                let v = arr[i].priceCurrent;
                min = (v < min) ? v : min;
                max = (v > max) ? v : max;
            }
            return [min, max];
        } else {
            return [0, 1];
        }
    }

    private getList(): Observable<CatalogModel> {
        return this.catalogService.getAll();
    }

    public onFilterChange(filter: IFilterParams): void {
        this.filterChange$.next(filter);
    }

    /*
        public openDialog(): Observable<EGender[]> {
            return this.dialog.open(CatalogListModalComponent).afterClosed();
        }*/
}
