export interface IStatisticInfo {
    peopleCount: number;
    purchasesCount: number;
}

export class StatisticInfoModel {
    public readonly peopleCount: number;
    public readonly purchasesCount: number;

    public constructor(_src?: IStatisticInfo) {
        this.peopleCount = _src && _src.peopleCount || 0;
        this.purchasesCount = _src && _src.purchasesCount || 0;
    }
}