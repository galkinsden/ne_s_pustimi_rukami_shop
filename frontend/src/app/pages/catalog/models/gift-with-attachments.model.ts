import {GiftModel, IGift} from "./gift.model";
import {AttachmentModel, IAttachment} from "./attachment.model";

export interface IAttachmentModel {
    id: string;
    attachments: IAttachment[];
}

export interface IGiftWithAttachments extends IGift {
    attachments: IAttachmentModel[];
}

export class GiftWithAttachmentsModel extends GiftModel {
    public readonly attachments: AttachmentModel[];
    constructor(giftModel: GiftModel, attachments: IAttachmentModel) {
        super(giftModel);
        this.attachments = attachments && attachments.attachments ? attachments.attachments.map((attachment: IAttachment) => new AttachmentModel(attachment)) : [];
    }

}