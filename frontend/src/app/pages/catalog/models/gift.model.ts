import {ECollections, EGender, EPackage} from '../catalog-controller/catalog-controller.component';
import {Moment} from 'moment';
import {IImg, ImgModel} from './img.model';
import {AttachmentModel, IAttachment} from './attachment.model';
import {CatalogImgModel, ICatalogImg} from "./catalog-img.model";
import {IMode, ModeModel} from "./mode.model";
import {SocialFullInfoModel} from "./social-full-Info.model";

export interface IGift {
    id: string;
    name: string;
    typeCollections: ECollections[];
    priceOld: number;
    priceCurrent: number;
    date: Moment;
    num: number;
    img: ICatalogImg;
    imgs: IImg[];
    genders: EGender[];
    packages: EPackage[];
    disabled: boolean;
    social?: SocialFullInfoModel
}

export class GiftModel {

    public readonly id: string;
    public readonly name: string;
    public readonly typeCollections: ECollections[];
    public priceOld: number;
    public priceCurrent: number;
    public readonly date: Moment;
    public readonly num: number;
    public readonly img: CatalogImgModel;
    public readonly imgs: ImgModel[];
    public readonly genders: EGender[];
    public readonly packages: EPackage[];
    public readonly disabled: boolean;
    public readonly social: SocialFullInfoModel;

    public constructor(_src: IGift) {
        this.id = _src && _src.id ? _src.id : '';
        this.name = _src && _src.name ? _src.name : '';
        this.typeCollections = _src && _src.typeCollections && _src.typeCollections.length ? _src.typeCollections : [];
        this.priceOld = _src && _src.priceOld ? _src.priceOld : null;
        this.priceCurrent = _src && _src.priceCurrent ? _src.priceCurrent : null;
        this.date = _src && _src.date ? _src.date : null;
        this.num = _src && _src.num ? _src.num : null;
        this.img = _src && _src.img ? new CatalogImgModel(_src.img) : new CatalogImgModel(null);
        this.imgs = _src && _src.imgs ? _src.imgs.map((img: IImg) => new ImgModel(img)) : [];
        this.genders = _src && _src.genders && _src.genders.length ? _src.genders : [];
        this.packages = _src && _src.packages && _src.packages.length ? _src.packages : [];
        this.disabled = _src && _src.disabled;
        this.social = _src && _src.social;

    }
}
