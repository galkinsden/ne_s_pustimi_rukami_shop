import {IImg, ImgModel} from "./img.model";

export interface ICatalogImg {
    small: IImg;
    large: IImg;
}

export class CatalogImgModel {

    public readonly small: ImgModel;
    public readonly large: ImgModel;

    public constructor(_src: ICatalogImg) {
        this.small = _src && _src.small && new ImgModel(_src.small) || new ImgModel(null);
        this.large = _src && _src.large && new ImgModel(_src.large) || new ImgModel(null);
    }
}
