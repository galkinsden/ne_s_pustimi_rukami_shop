import {GiftModel, IGift} from "./gift.model";

export interface ICatalog {
    list: IGift[];
}

export class CatalogModel {

    public readonly list: GiftModel[];

    public constructor(_src: ICatalog) {
        this.list = _src && _src.list ? _src.list.map((g: IGift) => new GiftModel(g)) : [];
    }
}
