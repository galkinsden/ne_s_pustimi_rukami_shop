export enum EModes {
    hit = 'hit',
    sale = 'sale',
    empty = 'empty'
}

export interface ISocialFullInfo {
    id: string;
    view?: number;
    like?: number;
    purchases?: number;
    comments?: string[];
    modes: EModes[];
}

export class SocialFullInfoModel {
    public readonly id: string;
    public view: number;
    public like: number;
    public purchases: number;
    public comments: string[];
    public modes: EModes[];

    public constructor(_src?: ISocialFullInfo) {
        this.id = _src && _src.id || '';
        this.view = _src && _src.view || 0;
        this.like = _src && _src.like || 0;
        this.purchases = _src && _src.purchases || 0;
        this.comments = _src && Array.isArray(_src.comments) && _src.comments || [];
        this.modes = _src && Array.isArray(_src.modes) && _src.modes || [];
    }
}