export interface IImg {
    img: string;
    id: string;
}

export class ImgModel {

    public readonly id: string;
    public readonly img: string;

    public constructor(_src: IImg) {
        this.id = _src && _src.id || '';
        this.img = _src && _src.img || '';
    }
}
