export interface IMode {
    name: string;
}

export class ModeModel {

    public readonly name: string;

    public constructor(_src: IMode) {
        this.name = _src && _src.name || '';
    }
}
