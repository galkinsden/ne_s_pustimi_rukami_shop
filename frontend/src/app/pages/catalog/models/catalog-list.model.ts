
import {GiftModel} from "./gift.model";

export class CatalogListModel {

    public constructor(
        public readonly list: GiftModel[],
        public readonly minSlider: number,
        public readonly maxSlider: number,
        public readonly modalGiftId: string
    ) {

    }
}
