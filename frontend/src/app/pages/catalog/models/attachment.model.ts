export interface IAttachment {
    name: string;
    count: number;
    price?: number
}

export class AttachmentModel {

    public readonly name: string;
    public readonly count: number;
    public readonly price: number;

    public constructor(_src: IAttachment) {
        this.name = _src && _src.name || '';
        this.count = _src && _src.count || 1;
        this.price = _src && _src.price;
    }
}
