﻿import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {IMailSendRequest, MailService} from "../../modules/mail.service";
import {StatisticService} from "./statistic.service";
import {Observable} from "rxjs/Observable";
import {StatisticInfoModel} from "./models/statistic-info.model";

@Component({
    moduleId: module.id,
    templateUrl: 'catalog.component.html',
    styleUrls: ['./catalog.component.scss']
})

export class CatalogComponent implements OnInit{

    public $model: Observable<StatisticInfoModel>;
    public static PATH: string = 'catalog';

    testForm: FormGroup;
    address = '';
    date = '';
    comment = '';
    name = '';
    phone = '';
    emial = '';

    ngOnInit() {
        this.$model = this.statisticService.getStatistic();
    }

    constructor(private fb: FormBuilder, private mailService: MailService, private statisticService: StatisticService) {

        this.testForm = fb.group({
            'name': '',
            'address': '',
            'date': '',
            'comment': '',
            'phone': '',
            'email': '',
        });

    }
}
