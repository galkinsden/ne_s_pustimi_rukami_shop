import {NgModule, ModuleWithProviders} from '@angular/core';
import {CatalogComponent} from './catalog.component';
import {CatalogControllerComponent} from './catalog-controller/catalog-controller.component';
import {CatalogListComponent} from './catalog-list/catalog-list.component';
import {CatalogListItemComponent} from './catalog-list/catalog-list-item/catalog-list-item.component';
import {CatalogService} from './catalog.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ArrayCheckboxesModule} from '../../modules/array-checkboxes/array-checkboxes.module';
import {CatalogControllerService} from './catalog-controller/catalog-conroller.service';
import {CommonModule} from '@angular/common';
import { NouisliderModule } from 'ng2-nouislider';
import {
    MatButtonModule, MatCheckboxModule, MatDialogModule, MatExpansionModule, MatFormFieldModule, MatIconModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule
} from '@angular/material';
import {SafeImgPipeModule} from '../../modules/safe-img/safe-img.module';
import {CatalogListItemModalComponent} from './catalog-list/catalog-list-item/catalog-list-item-modal/catalog-list-item-modal.component';
import {QuickViewModule} from "../../modules/quick-view/quick-view.module";
import {EmptyAreaModule} from "../../modules/empty-area/empty-area.module";
import {PreloaderModule} from "../../modules/preloader/preloader.module";
import {MailService} from "../../modules/mail.service";
import {SocialService} from "./social.service";
import {StatisticService} from "./statistic.service";


@NgModule({
    declarations: [
        CatalogComponent,
        CatalogControllerComponent,
        CatalogListComponent,
        CatalogListItemComponent,
        CatalogListItemModalComponent
    ],
    exports     : [
        CatalogComponent,
        CatalogControllerComponent,
        CatalogListComponent,
        CatalogListItemComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ArrayCheckboxesModule,
        NouisliderModule,
        MatFormFieldModule,
        MatSelectModule,
        MatOptionModule,
        MatIconModule,
        MatDialogModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        SafeImgPipeModule,
        QuickViewModule,
        MatExpansionModule,
        EmptyAreaModule,
        MatCheckboxModule,
        PreloaderModule
    ],
    providers: [
        CatalogService,
        CatalogControllerService,
        MailService,
        SocialService,
        StatisticService
    ],
    entryComponents: [CatalogListItemModalComponent, /*, CatalogListItemGaleryComponent*/]
})
export class CatalogModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: CatalogModule};
    }
}