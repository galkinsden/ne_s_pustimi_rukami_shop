﻿import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: 'shipping-and-payment.component.html',
    styleUrls: ['./shipping-and-payment.component.scss']
})

export class ShippingAndPaymentComponent {

    public static PATH: string = 'shipping-and-payment';

    constructor() {}
}
