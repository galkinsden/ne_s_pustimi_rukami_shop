﻿import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: 'corporate.component.html',
    styleUrls: ['./corporate.component.scss']
})

export class CorporateComponent {

    public static PATH: string = 'corporate';

    constructor() {}
}
