﻿import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: 'instagram.component.html',
    styleUrls: ['./instagram.component.scss']
})

export class InstagramComponent {

    public static PATH: string = 'instagram';

    constructor() {}
}
