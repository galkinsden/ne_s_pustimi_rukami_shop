import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Observable} from "rxjs/Observable";

export enum ESaleTypes {
    like = 'like'
}

export interface IPurchase {
    id: string;
    count: number;
    saleTypes?: ESaleTypes[]
}

@Injectable()
export class BusketService {
    private purchases: BehaviorSubject<IPurchase[]> = new BehaviorSubject([]);

    constructor() {
        if(localStorage.purchases) {
            try{
                this.setPurchases(JSON.parse(localStorage.purchases));
            } catch (e)  {

            }

        }
    }

    public getObservable(): Observable<IPurchase[]> {
        return this.purchases.asObservable();
    }

    public getPurchases(): IPurchase[] {
        return this.purchases.getValue();
    }

    public setPurchase(value: IPurchase): Observable<boolean> {
        try {
            let purchases: IPurchase[] = this.purchases.getValue();
            let finded: IPurchase = purchases.find(purchase => purchase.id == value.id);
            if(finded) {
                finded.count += value.count;
            } else {
                purchases.push(value);
            }
            this.setPurchases(purchases);
            return Observable.of(true);
        } catch (error) {
            return Observable.of(false);
        }
    }

    private setPurchases(value: IPurchase[]): void {
        this.purchases.next(value);
        this.setToLocalStorage();
    }

    public setCountToPurchase(id: string, count: number): void {
        let purchases: IPurchase[] = this.purchases.getValue();
        let currentPurchases: IPurchase = purchases.find(purchase => purchase.id === id);
        if(currentPurchases) {
            currentPurchases.count = count;
            this.setPurchases(purchases);
        } else {
            this.setPurchase({id: id, count: count});
        }
    }

    public deletePurchase(id: string): void {
        let purchases: IPurchase[] = this.purchases.getValue();
        purchases = purchases.filter(purchase => purchase.id !== id);
        this.setPurchases(purchases);
    }

    private setToLocalStorage() {
        localStorage.purchases = JSON.stringify(this.purchases.getValue());
    }

    public deleteAllPurchases(): void {
       this.setPurchases([]);
    }
}