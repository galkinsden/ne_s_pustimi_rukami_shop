import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {
    MatAccordion, MatDatepickerModule, MatExpansionModule,
    MatFormFieldModule, MatIconModule, MatInputModule, MatProgressBarModule, MatRadioModule, MatSnackBarModule,
    MatStepperModule
} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {YaCoreModule} from "angular2-yandex-maps";
import {HttpClientModule} from "@angular/common/http";
import {BusketComponent} from "./busket.component";
import {BusketPostService} from "./busket-post.service";
import {ProductComponent} from "./product/product.component";
import {PersonComponent} from "./person/person.component";
import {DeliveryComponent} from "./delivery/delivery.component";
import {UnSnackBarModule} from "../../snackbar/un-snack-bar.module";
import {MailService} from "../../mail.service";
import {PreloaderModule} from "../../preloader/preloader.module";
import {SocialService} from "../../../pages/catalog/social.service";

@NgModule({
    declarations: [BusketComponent, DeliveryComponent, PersonComponent, ProductComponent],
    exports: [BusketComponent],
    providers: [BusketPostService, MailService, SocialService],
    imports: [
        CommonModule,
        MatStepperModule,
        MatProgressBarModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        YaCoreModule.forRoot(),
        HttpClientModule,
        UnSnackBarModule,
        MatRadioModule,
        MatIconModule,
        MatExpansionModule,
        MatSnackBarModule,
        MatDatepickerModule,
        PreloaderModule
    ],
})
export class BusketModule {
}
