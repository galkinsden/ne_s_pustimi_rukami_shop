import {Component, OnDestroy, OnInit} from "@angular/core";
import {BusketPostService} from "../busket-post.service";
import {FormGroup} from "@angular/forms";

@Component({
    selector: 'person',
    templateUrl: 'person.component.html',
    styleUrls: ['person.component.scss']
})
export class PersonComponent implements OnInit, OnDestroy {
    public personalData: FormGroup;
    constructor(private busketPostService: BusketPostService) {}
    ngOnInit() {
        this.personalData = this.busketPostService.getPersonalDataFB();
    }
    ngOnDestroy() {}
}