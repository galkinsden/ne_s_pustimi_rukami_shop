import {Injectable} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs/Observable";
import * as moment from 'moment';
import {IMailSendRequest, MailService} from "../../mail.service";
import {BusketService, ESaleTypes, IPurchase} from "../busket.service";
import {SocialService} from "../../../pages/catalog/social.service";

@Injectable()
export class BusketPostService {
    private addressInfoFB: FormGroup;
    private deliveryData: string;
    private personInfo: string;
    private products: string;
    private personalDataFB: FormGroup;
    private productFB: FormGroup;
    private resultPrice: number;
    private purchaseLenght: number;

    constructor(private fb: FormBuilder,
                private mailService: MailService,
                private busketService: BusketService,
                protected socialService: SocialService
                ) {
        this.addressInfoFB = this.createDeliveryFormGroup();
        this.personalDataFB = this.createPersonalDataFormGroup();
        this.productFB = this.createProductFormGroup();
    }

    public setDeliveryData(value: string): void {
        this.deliveryData = value;
    }

    public setPersonInfo(value: string): void {
        this.personInfo = value;
    }

    public setProducts(value: string): void {
        this.products = value;
    }

    private createDeliveryFormGroup(): FormGroup {
        return this.fb.group({
            'address': ['', Validators.required],
            'placeType': [''],
            'additional': [''],
            'date': ['', Validators.required]
        })
    }

    private createPersonalDataFormGroup(): FormGroup {
        return this.fb.group({
            'name': ['', Validators.required],
            'phone': ['', Validators.required]
        });
    }

    private createProductFormGroup(): FormGroup {
        return this.fb.group({
            'products': [ '', [Validators.required]],
        });
    }

    public getAddressInfoFB(): FormGroup {
        return this.addressInfoFB
    }

    public getPersonalDataFB(): FormGroup {
        return this.personalDataFB;
    }

    public getProductFB(): FormGroup {
        return this.productFB;
    }

    public setProductStatus(value: boolean): void {
        this.productFB.get('products').setValue(value ? true : '');
    }

    public getResultPrice(): number {
        return this.resultPrice;
    }

    public getPurchaseLength(): number {
        return this.purchaseLenght;
    }

    public setResultPrice(val: number): void {
         this.resultPrice = val;
    }

    public setPurchaseLength(val: number): void {
         this.purchaseLenght = val;
    }

    public buy(): Observable<boolean> {
        let mailMessage: IMailSendRequest = {
            address: this.addressInfoFB.get('address').value,
            comment: this.addressInfoFB.get('additional').value,
            date: this.addressInfoFB.get('date').value,
            gifts: this.busketService.getPurchases(),
            name: this.personalDataFB.get('name').value,
            phone: this.personalDataFB.get('phone').value,
            placeType: this.addressInfoFB.get('placeType').value
        };
        mailMessage.gifts = this.addSales(mailMessage);
        return this.mailService.sendMail(mailMessage);
    }

    private addSales(mailMessage: IMailSendRequest): IPurchase[] {
        return mailMessage.gifts.map(gift => {
            if(this.socialService.isLiked(gift.id)) {
                if(Array.isArray(gift.saleTypes)) {
                    gift.saleTypes.push(ESaleTypes.like)
                } else {
                    gift.saleTypes = [ESaleTypes.like]
                }
            }
            return gift;
        });
    }

}