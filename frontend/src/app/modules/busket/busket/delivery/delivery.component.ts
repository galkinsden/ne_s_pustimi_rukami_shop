import {Component, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs/Subscription";
import {Observable} from "rxjs/Observable";
import {BusketPostService} from "../busket-post.service";
import * as moment from 'moment';
import {Moment} from "moment";

@Component({
    selector: 'delivery',
    templateUrl: 'delivery.component.html',
    styleUrls: ['delivery.component.scss'],
    encapsulation : ViewEncapsulation.None
})
export class DeliveryComponent implements OnInit, OnDestroy {
    public addressInfo: FormGroup;
    public subscriberAddressChanged: Subscription;
    public points: any[] = [];
    public minDatepicker: Moment = moment().add(2,'days');

    constructor(private http: HttpClient, private busketPostService: BusketPostService) {}

    public ngOnInit(): void {
        this.addressInfo = this.busketPostService.getAddressInfoFB();

        this.subscriberAddressChanged = this.addressInfo.get('address').valueChanges
            .debounceTime(300)
            .distinctUntilChanged()
            .switchMap(val => this.getPoints(val))
            .subscribe((elem: any) => {
                this.savePoints(elem);
            });
    }

    /**
     * Получаем точки от Яндекса
     * @param elem
     */
    private savePoints(elem: any): void {
        this.points = elem.response.GeoObjectCollection.featureMember.map((point) => {
            return {
                lng: point.GeoObject.Point.pos.split(' ')[0],
                lat: point.GeoObject.Point.pos.split(' ')[1],
                balloonHeader: point.GeoObject.metaDataProperty.GeocoderMetaData.text,
                balloonBody: 'Выбрать>>'
            }
        })
    }

    /**
     * Запрос к Яндексу
     * @param {string} val
     * @returns {Observable<any>}
     */
    private getPoints(val: string): Observable<any> {
        return this.http.get('https://geocode-maps.yandex.ru/1.x/', {
            params: {
                format: 'json',
                geocode: val
            }
        });
    }

    ngOnDestroy() {
        this.subscriberAddressChanged && this.subscriberAddressChanged.unsubscribe();
    }
}