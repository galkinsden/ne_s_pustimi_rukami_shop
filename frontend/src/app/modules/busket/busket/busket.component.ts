import {Component, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";
import {BusketService, IPurchase} from "../busket.service";
import {ISubscription, Subscription} from "rxjs/Subscription";
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {ImgModel} from "../../../pages/catalog/models/img.model";
import {BusketPostService} from "./busket-post.service";
import {Router} from "@angular/router";
import {MainComponent} from "../../../pages/main/main-page.component";
import {UnSnackBarService} from "../../snackbar/un-snack-bar.service";
import {StateModel} from "../../state/state.model";
import {EStateModel} from "../../state/state-model.enum";
import {CatalogComponent} from "../../../pages/catalog/catalog.component";
import {ContactsComponent} from "../../../pages/contacts/contacts.component";
import {ICatalogImg} from "../../../pages/catalog/models/catalog-img.model";
import {GoogleAnalyticsEventsService} from "../../../google-analytics-events.service";
import {SocialService} from "../../../pages/catalog/social.service";
export interface IPurchaseFull {
    id: string;
    name: string;
    count: number;
    price: number;
    img: ICatalogImg;
}

@Component({
    selector: 'busket',
    templateUrl: 'busket.component.html',
    styleUrls: ['busket.component.scss']
})
export class BusketComponent extends StateModel implements OnInit, OnDestroy {

    private buySubscription: Subscription = null;

    public constructor(
        public busketPostService: BusketPostService,
        protected busketService: BusketService,
        protected router: Router,
        protected snackBar: UnSnackBarService,
        protected googleAnalyticsEventsService: GoogleAnalyticsEventsService,
        protected socialService: SocialService
    ) {
        super(EStateModel.NONE);
    }

    public ngOnInit(): void {
    }

    public ngOnDestroy(): void {
        this.buySubscription && this.buySubscription.unsubscribe();
    }

    public getProgressValue(): number {
        let cost = 10;
        if(!this.busketPostService.getResultPrice()) {
            return 10;
        }
        if(this.busketPostService.getResultPrice()) {
            cost += 50;
        }

        if(this.busketPostService.getPersonalDataFB().valid) {
            cost += 20;
        }

        if(this.busketPostService.getAddressInfoFB().valid) {
            cost += 20;
        }

        return cost;
    }

    buy() {
        this.googleAnalyticsEventsService.emitEvent("Busket", "clickBuy", "Покупка!", 1);
        if (this.busketPostService.getAddressInfoFB().valid) {
            this.state = EStateModel.LOADING;
            this.buySubscription = this.busketPostService
                .buy()
                .subscribe(
                    (result) => {
                        if(result) {
                            console.log('result: ', result);
                            this.busketService.getPurchases().forEach((element: IPurchase) => {
                                let subscription = this.socialService.setPurchasesById(element.id, element.count).subscribe(() => {
                                }, () => {}, () => {
                                    subscription.unsubscribe();
                                    this.busketService.deleteAllPurchases();
                                    this.snackBar.openSucces('Данные успешно отправлены! Мы свяжемся с вами в близжайшее время');
                                    this.router.navigate([CatalogComponent.PATH]);
                                });
                            });
                        } else {
                            this.snackBar.openError('Данные не отправлены! Попробуйте позже или свяжитесь со службой поддержки!', '', {
                                duration: 5000
                            });
                            this.router.navigate([ContactsComponent.PATH]);
                        }
                        this.state = EStateModel.LOADED;
                    },
                    () => {
                        this.state = EStateModel.ERROR;
                    });
        }
    }

    doFirstStep() {
        this.googleAnalyticsEventsService.emitEvent("Busket", "clickBusketFirstStep", "Персональная информация", 1);
    }

    doSecondStep() {
        this.googleAnalyticsEventsService.emitEvent("Busket", "clickBusketSecondStep", "Доставка", 1);
    }

}