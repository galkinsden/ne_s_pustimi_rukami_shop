import {Component, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";
import {IPurchaseFull} from "../busket.component";
import {BusketPostService} from "../busket-post.service";
import {BusketService, IPurchase} from "../../busket.service";
import {IGift} from "../../../../pages/catalog/models/gift.model";
import {ISubscription} from "rxjs/Subscription";
import {CatalogService} from "../../../../pages/catalog/catalog.service";
import {Observable} from "rxjs/Observable";
import {SocialService} from "../../../../pages/catalog/social.service";

@Component({
    selector: 'product',
    templateUrl: 'product.component.html',
    styleUrls: ['product.component.scss'],
    encapsulation : ViewEncapsulation.None
})
export class ProductComponent implements OnInit, OnDestroy {
    public purchases: IPurchaseFull[] = [];
    public resultPrice: number = 0;
    public subscriberBusket: ISubscription;

    public constructor(
        private busketService: BusketService,
        private busketPostService: BusketPostService,
        private catalogService: CatalogService,
        private socialService: SocialService
    ) {
    }

    public ngOnInit(): void {
        this.subscriberBusket = this.busketService.getObservable()
            .switchMap((value: IPurchase[]) => this.appendToBusket(value))
            .subscribe((value: number) => {
                this.busketPostService.setProductStatus(!!value)
            })
    }

    public ngOnDestroy(): void {
        this.subscriberBusket.unsubscribe();
    }

    /**
     * Добавляем в массив элементов, возвращаем цену
     * @param {IPurchase} elem
     */
    private appendToBusket(value: IPurchase[]): Observable<number> {
        let tmpResult: number = 0;
        return Observable.of(value)
            .map((value: IPurchase[]) => {
               return value.map(elem => {
                    let alreadyInBusket: IPurchaseFull = this.purchases.find(purchase => purchase.id == elem.id);
                    if(alreadyInBusket) {
                        alreadyInBusket.count = elem.count;
                        tmpResult += alreadyInBusket.count * alreadyInBusket.price;
                        return;
                    }
                    return elem;
                }).filter(elem => !!elem);
            })
            .switchMap((value: IPurchase[]) => {
                if(!value.length) {return Observable.of(tmpResult);}
                let ids = value.map(elem => elem.id);
                return this.catalogService.getGiftsByIds(ids).map((gifts:IGift[]) => {
                    value.map((elem: IPurchase) => {
                        let finded: IGift =  gifts.find(listElem => listElem.id == elem.id);
                        finded.priceCurrent = this.socialService.isLiked(finded.id) ?   Math.round(finded.priceCurrent * 0.99) : finded.priceCurrent;
                        this.purchases.push(this.createPurchaseFull(finded, elem.count));
                        tmpResult += elem.count * finded.priceCurrent;
                    });
                    return tmpResult;
                })
            })
            .map((tmpResult: number) => {
                this.busketPostService.setPurchaseLength(this.purchases.length);
                this.busketPostService.setResultPrice(tmpResult);
                this.resultPrice =  this.busketPostService.getResultPrice();
                return this.purchases.length
            })


    }

    /**
     * Создаем сущность для работы с корзиной
     * @param {IGift} gift
     * @param {number} count
     * @returns {IPurchaseFull}
     */
    private createPurchaseFull(gift: IGift, count: number): IPurchaseFull {
        return {
            id: gift.id,
            name: gift.name,
            count: count,
            price: gift.priceCurrent,
            img: gift.img
        }
    }

    /**
     * Удаление из корзины
     * @param {string} id
     */
    public cutFromBusket(id: string): void {
        this.purchases = this.purchases.filter(element => element.id != id);
        this.busketService.deletePurchase(id);
        this.busketPostService.setProductStatus(!!this.purchases.length)
    }

    /**
     * Обработка изменения кол-ва
     * @param {number} count
     * @param {string} id
     */
    public onChangeCount(count: number, id: string) {
        this.busketService.setCountToPurchase(id, +count);
    }

    public increment(value: number, id: string): void {
       if(value >= 1) {
           this.onChangeCount(value, id);
       }
    }
}