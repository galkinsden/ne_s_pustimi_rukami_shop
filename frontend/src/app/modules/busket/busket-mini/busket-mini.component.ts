import {Component, OnInit, ViewEncapsulation} from "@angular/core";
import {BusketService, IPurchase} from "../busket.service";
import {Subscription} from "rxjs/Subscription";
import {IGift} from "../../../pages/catalog/models/gift.model";
import {Router} from "@angular/router";
import {BusketComponent} from "../../../pages/busket/busket.component";
import {CatalogService} from "../../../pages/catalog/catalog.service";
import {Observable} from "rxjs/Observable";
import {GoogleAnalyticsEventsService} from "../../../google-analytics-events.service";
import {SocialService} from "../../../pages/catalog/social.service";

interface IBusketMiniItem {
    img: string;
    name: string;
    count: number;
    price: number;
}

@Component({
    selector: 'busket-mini',
    templateUrl: 'busket-mini.component.html',
    styleUrls: ['busket-mini.component.scss'],
    encapsulation: ViewEncapsulation.None,
    /* tslint:disable:use-host-property-decorator*/
    host: {
        'class': 'busket-mini'
    }
    /* tslint:enable */
})

export class BusketMiniComponent implements OnInit {
    // public subscriberBusket: Subscription;
    // public items: IBusketMiniItem[] = [];
    public $model: Observable<IBusketMiniItem[]>;

    constructor(private busketService: BusketService,
                private router: Router,
                private catalogService: CatalogService,
                protected googleAnalyticsEventsService: GoogleAnalyticsEventsService,
                private socialService: SocialService) {
    }

    public ngOnInit(): void {
        this.$model = this.busketService.getObservable()
            .switchMap((purchases: IPurchase[]) => {
                let ids: string[] = purchases.map(elem => elem.id);
                if(!ids.length) {
                    return Observable.of([]);
                }
                return this.catalogService.getGiftsByIds(ids).map((gifts: IGift[]) => {
                    return purchases.map(elem => {
                        let finded: IGift =  gifts.find(listElem => listElem.id == elem.id);
                        finded.priceCurrent = this.socialService.isLiked(finded.id) ?  Math.round(finded.priceCurrent * 0.99) : finded.priceCurrent;
                        return {img: finded.img.small.img, name: finded.name, count: elem.count, price: finded.priceCurrent};
                    });
                })
            })
    }

    moveToBusket() {
        this.googleAnalyticsEventsService.emitEvent("Busket", "clickMiniBusket", "Оформить заказ", 1);
        this.router.navigate([BusketComponent.PATH])
    }
}