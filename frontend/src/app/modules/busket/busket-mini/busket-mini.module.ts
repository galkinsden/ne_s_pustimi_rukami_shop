import {NgModule, OnInit} from "@angular/core";
import {BusketMiniComponent} from "./busket-mini.component";
import {MatIconModule} from "@angular/material";
import {SvgIconModule} from "../../svg-icon/svg-icon.module";
import {CommonModule} from "@angular/common";
import {MatMenuModule} from '@angular/material/menu';
import {CatalogService} from "../../../pages/catalog/catalog.service";
import {SocialService} from "../../../pages/catalog/social.service";

@NgModule({
    declarations: [BusketMiniComponent],
    exports: [BusketMiniComponent],
    providers: [CatalogService, SocialService],
    imports: [CommonModule, SvgIconModule, MatMenuModule],
})
export class BusketMiniModule {
}
