import {NgModule, ModuleWithProviders} from '@angular/core';
import {SvgIconModule} from '../svg-icon/svg-icon.module';
import {SocialBarComponent} from "./social-bar.component";

@NgModule({
    declarations: [SocialBarComponent],
    exports     : [SocialBarComponent],
    imports     : [SvgIconModule],
    providers   : []
})

export class SocialBarModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: SocialBarModule};
    }
}
