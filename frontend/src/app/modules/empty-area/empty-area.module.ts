import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {EmptyAreaComponent} from './empty-area.component';

/**
 *  Модуль компонента замещающего сообщения
 */
@NgModule({
	declarations: [EmptyAreaComponent],
	exports: [EmptyAreaComponent],
	imports: [CommonModule, FormsModule],
	providers: [],
	entryComponents: [],
})
export class EmptyAreaModule {}
