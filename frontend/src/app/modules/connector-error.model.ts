import {EStatusCode} from "./status-code.enum";

export interface IError {
    code: string; // Код исключения
    description: string; // Описание исключения
    type: string; // Тип исключения
    status: EStatusCode;
}

/**
 * Модель ошибки коннектора
 * @class ConnectorErrorModel
 */
export class ConnectorErrorModel implements IError {

	public readonly code: string = '';
    public readonly description: string = '';
    public readonly type: string = '';
    public readonly status: EStatusCode = null;

	constructor(data: IError) {
        this.code = data && data.code;
        this.description = data && data.description;
        this.type = data && data.type;
        this.status = data && data.status;
	}

}
