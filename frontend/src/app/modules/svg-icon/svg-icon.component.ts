import { Component, ElementRef, Input, OnInit, Renderer2, ViewEncapsulation } from '@angular/core';

import { SvgIconRegistryService } from './svg-icon-registry.service';

@Component({
	selector: 'svg-icon',
	styleUrls: ['./svg-icon.component.scss'],
	template: '<ng-content></ng-content>',
})

export class SvgIconComponent implements OnInit {
	@Input() public set src(iconUrl: string) {
		this.iconReg.loadSvg(iconUrl).subscribe((value) => {
			this.setSvg(value);
		});
	}

	@Input() public set type(value: string) {
		this.setSvg(this.iconReg.get(value));
	}

	constructor(
		public element: ElementRef,
		private renderer: Renderer2,
		private iconReg: SvgIconRegistryService
	) {}

	public ngOnInit() {}

	private setSvg(value: string) {
		this.renderer.setProperty(this.element.nativeElement, 'innerHTML', value);
	}
}
