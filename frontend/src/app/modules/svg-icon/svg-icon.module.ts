import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SvgIconComponent} from './svg-icon.component';
import {SvgIconRegistryService} from './svg-icon-registry.service';

@NgModule({
	declarations    : [SvgIconComponent],
	exports         : [SvgIconComponent],
	providers       : [SvgIconRegistryService],
	imports         : [CommonModule],
	entryComponents : [SvgIconComponent]
})
export class SvgIconModule {
	public static forRoot(): ModuleWithProviders {
		return {ngModule: SvgIconModule};
	}
}
