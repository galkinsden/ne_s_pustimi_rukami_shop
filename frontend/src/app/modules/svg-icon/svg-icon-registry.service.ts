import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import {HttpClient} from "@angular/common/http";

const logo = require('!!raw-loader!../svg/logo.svg');
const logoBlackBig = require('!!raw-loader!../svg/logo-black-full.svg');
const logoWhiteBig = require('!!raw-loader!../svg/logo-white-big.svg');
const logoNew = require('!!raw-loader!../svg/logo_new.svg');
const busket = require('!!raw-loader!../svg/busket.svg');
const logoWhite = require('!!raw-loader!../svg/logo-white.svg');
const logoBlack = require('!!raw-loader!../svg/logo-black.svg');
const logoInstagram = require('!!raw-loader!../svg/instagram.svg');
const logoTelegram = require('!!raw-loader!../svg/telegram.svg');
const logoWhatsapp = require('!!raw-loader!../svg/whatsapp.svg');
const logoWhatsappCircle = require('!!raw-loader!../svg/whatsapp-circle.svg');
const logoTelegramCircle = require('!!raw-loader!../svg/telegram-circle.svg');
const logoInstagramCircle = require('!!raw-loader!../svg/instagram-circle.svg');

@Injectable()
export class SvgIconRegistryService {

	private iconsByUrl = new Map<string, string>();
	private iconsLoadingByUrl = new Map<string, Observable<string>>();

	private icons: Object = {
		'logo': logo,
		'logo-new': logoNew,
		'busket': busket,
		'logo-white': logoWhite,
		'logo-black': logoBlack,
		'logo-instagram': logoInstagram,
		'logo-telegram': logoTelegram,
		'logo-whatsapp': logoWhatsapp,
		'instagram-circle': logoInstagramCircle,
		'telegram-circle': logoTelegramCircle,
		'whatsapp-circle': logoWhatsappCircle,
		'logo-black-big': logoBlackBig,
		'logo-white-big': logoWhiteBig,
	};

	constructor(private http: HttpClient) {}

	public getIcons() {
		return this.icons;
	}

	public loadSvg(url: string): Observable<string> {
		if (this.iconsByUrl.has(url)) {
			return Observable.of(this.iconsByUrl.get(url));
		} else if (this.iconsLoadingByUrl.has(url)) {
			return this.iconsLoadingByUrl.get(url);
		} else {
			const o = this.http.get(url)
				.map((res: Response) => {
					return res.text();
				})
				.do((item) => {
					this.iconsByUrl.set(url, item);
				})
				.finally(() => {
					this.iconsLoadingByUrl.delete(url);
				})
				.share();

			this.iconsLoadingByUrl.set(url, o);
			return o;
		}
	}

	public get(name: string): string {
		return this.icons[name] || this.onError(name);
	}

	private onError(name: string): string {
		return console.error(`Error: svg-icon "${name}" not found.`) || '';
	}
}
