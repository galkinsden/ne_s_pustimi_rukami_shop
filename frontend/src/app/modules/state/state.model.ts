import {EStateModel} from './state-model.enum';

export class StateModel {
    public state: EStateModel = EStateModel.NONE;

    constructor(state: EStateModel) {
        this.state = state;
    }

    public isStateNone(): boolean {
        return this.state === EStateModel.NONE;
    }

    public isStateLoading(): boolean {
        return this.state === EStateModel.LOADING;
    }

    public isStateLoaded(): boolean {
        return this.state === EStateModel.LOADED;
    }

    public isStateError(): boolean {
        return this.state === EStateModel.ERROR;
    }

    public isStateClosed(): boolean {
        return this.state === EStateModel.CLOSED;
    }

    public isStateWait(): boolean {
        return this.state === EStateModel.WAIT;
    }
}
