import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {HttpClient} from "@angular/common/http";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {MailConnector} from "../connectors/mail.connector";
import {IPurchase} from "./busket/busket.service";

export interface IMailSendRequest {
    address: string;
    date: string;
    comment: string;
    name: string;
    phone: string;
    email?: string;
    gifts: IPurchase[];
    placeType: string;
}

@Injectable()
export class MailService {

    private mailConnector: MailConnector;

    public constructor(
        protected http: HttpClient
    ) {
        this.mailConnector = new MailConnector(http);
    }

    public sendMail(data: IMailSendRequest): Observable<boolean> {
        return this.mailConnector.sendMail(data)
            .map((res: boolean) => res);
    }
}