import {NgModule, ModuleWithProviders} from '@angular/core';
import {SafeImgPipe} from './safe-img.pipe';

@NgModule({
    declarations: [SafeImgPipe],
    exports     : [SafeImgPipe],
})
export class SafeImgPipeModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: SafeImgPipeModule};
    }
}
