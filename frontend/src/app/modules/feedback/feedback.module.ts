import {FeedbackComponent} from "./feedback.component";
import {NgModule} from "@angular/core";
import {MatFormFieldModule, MatInputModule} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

@NgModule({
    declarations: [
        FeedbackComponent,
    ],
    exports: [
        FeedbackComponent,
    ],
    imports: [
        MatFormFieldModule,
        CommonModule, FormsModule, ReactiveFormsModule,
        MatInputModule
    ],
})
export class FeedbackModule {
}
