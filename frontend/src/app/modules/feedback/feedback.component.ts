import {Component} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'feedback',
    templateUrl: 'feedback.component.html',
    styleUrls: ['feedback.component.scss'],
})
export class FeedbackComponent {
    feedback: FormGroup;

    constructor(fb: FormBuilder) {
        this.feedback = fb.group({
            'name': ['', Validators.required],
            'email': '',
            'phone': '',
            'message': '',
        }, {validator: this.validateFeedback});
    }

    public onSubmit(form: FormGroup): void {
        console.log(form);
    }

    public validateFeedback(formGroup): any {
        let email = formGroup.controls['email']; //access any of your form fields like this
        let phone = formGroup.controls['phone']; //access any of your form fields like this
        if(!email.value && !phone.value) {
            email.status = 'INVALID';
            phone.status = 'INVALID';
            return { notContact: true }
        } else {
            email.status = 'VALID';
            phone.status = 'VALID';
            return null;
        }
    }
}