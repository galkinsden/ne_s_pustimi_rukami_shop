import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {QuickViewComponent} from './quick-view.component';

@NgModule({
	declarations: [QuickViewComponent],
	exports     : [QuickViewComponent],
	imports     : [CommonModule],
})
export class QuickViewModule {
	public static forRoot(): ModuleWithProviders {
		return {ngModule: QuickViewModule};
	}
}
