import {Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
	selector        : 'quick-view',
	templateUrl     : './quick-view.component.html',
	styleUrls       : ['./quick-view.component.scss'],
})
export class QuickViewComponent {

    @Input() public title: string = 'Быстрый просмотр';
    @Input() public open: boolean = false;
}
