import {Pipe, PipeTransform} from '@angular/core';

/**
 * Экранирует специальные символы
 * @param {string} str
 * @return {string}
 */
function escapeRegExp(str: string): string {
	return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
}

/*
 * Фильтр для выделения текста
 * Пример использования:
 * `<span [innerHTML]="'Text to highlight' | highlightText: 'highlight': 'markClassName': true"></span>`
 */
@Pipe({
	name: 'highlightText'
})
export class HighlightTextPipe implements PipeTransform {

	public transform(input: string, markStr: string, markClass: string = 'is-extra', withSpaces: boolean = false): any {
		if (input && markStr) {
			const safeString: string = escapeRegExp(markStr);
			const regex = withSpaces
				? new RegExp(safeString.split('').join('\\s*'), 'gi')
				: new RegExp(safeString, 'gi');
			return input.replace(regex, `<mark class="${markClass}">$&</mark>`);
		}

		return input;
	}
}
