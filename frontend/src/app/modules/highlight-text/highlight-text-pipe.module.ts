import {NgModule} from '@angular/core';
import {HighlightTextPipe} from './highlight-text.pipe';
import {CommonModule} from '@angular/common';

@NgModule({
	imports: [
		CommonModule,
	],
	declarations: [
		HighlightTextPipe
	],
	exports: [
		HighlightTextPipe,
	]
})
export class HighlightTextPipeModule {

}
