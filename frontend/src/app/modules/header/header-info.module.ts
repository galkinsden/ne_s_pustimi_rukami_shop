import {NgModule, ModuleWithProviders} from '@angular/core';
import {HeaderInfoComponent} from "./header-info.component";
import {SvgIconModule} from "../svg-icon/svg-icon.module";

@NgModule({
    declarations: [HeaderInfoComponent],
    exports     : [HeaderInfoComponent],
    imports     : [SvgIconModule],
    providers   : []
})

export class HeaderInfoModule {
    public static forRoot(): ModuleWithProviders {
        return {ngModule: HeaderInfoModule};
    }
}
