import {Component, Input, ViewEncapsulation} from '@angular/core';
import {IMenu} from './menu-item.interface';
import { Router } from '@angular/router';

@Component({
	selector		: 'menu-item',
	templateUrl		: './menu-item.component.html',
	encapsulation	: ViewEncapsulation.None,
	styleUrls		: ['./menu-item.component.scss'],
})
/**
 * Директива выпадающего древовидного подмеменю
 * @class MenuItemComponent
 * @directive menu-item
 */
export class MenuItemComponent {
	@Input() public item: IMenu = null;

	constructor (private router: Router) {

  }

	public onClick (child: IMenu): void {
	child.action && child.action() || this.router.navigate([child.url]);
  }

}
