import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {MenuComponent} from './menu/menu.component';
import {MenuItemComponent} from './menu-item/menu-item.component';
import {MatSidenavModule, MatToolbarModule} from "@angular/material";
import {SvgIconModule} from "../svg-icon/svg-icon.module";
import {BusketMiniModule} from "../busket/busket-mini/busket-mini.module";

@NgModule({
	declarations: [MenuComponent, MenuItemComponent],
	exports     : [MenuComponent, MenuItemComponent],
	imports     : [CommonModule, RouterModule, SvgIconModule, BusketMiniModule, MatToolbarModule, MatSidenavModule],
	providers   : []
})

export class MenuModule {
	public static forRoot(): ModuleWithProviders {
		return {ngModule: MenuModule};
	}
}
