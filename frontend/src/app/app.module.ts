import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from './app.component';
import {CommonModule, DatePipe} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';

import {PagesModule} from './pages/pages.module';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MainComponent} from "./pages/main/main-page.component";
import {CorporateComponent} from "./pages/corporate/corporate.component";
import {ShippingAndPaymentComponent} from "./pages/shipping-and-payment/shipping-and-payment.component";
import {FaqComponent} from "./pages/faq/faq.component";
import {InstagramComponent} from "./pages/instagram/instagram.component";
import {ContactsComponent} from "./pages/contacts/contacts.component";
import {HeaderInfoModule} from "./modules/header/header-info.module";
import {CatalogModule} from "./pages/catalog/catalog.module";
import {BusketModule} from "./modules/busket/busket/busket.module";
import {BusketService} from "./modules/busket/busket.service";
import {BusketComponent} from "./pages/busket/busket.component";
import {FaqModule} from "./pages/faq/faq.module";
import {UnSnackBarModule} from "./modules/snackbar/un-snack-bar.module";
import {UnSnackBarService} from "./modules/snackbar/un-snack-bar.service";
import {MenuModule} from "./modules/menu/menu.module";
import {ContactsModule} from "./pages/contacts/contacts.module";
import {SocialBarModule} from "./modules/social-bar/social-bar.module";
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from "@angular/material";
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from "@angular/material-moment-adapter";
import {GoogleAnalyticsEventsService} from "./google-analytics-events.service";


/**
 * Базовый модуль приложения
 */
@NgModule({
	declarations: [
        AppComponent,
        CorporateComponent,
        ShippingAndPaymentComponent,
		InstagramComponent,
        MainComponent,
		BusketComponent,
	],
	imports: [
        CommonModule,
		BrowserModule,
		FormsModule,
		HttpClientModule,
		NgbModule.forRoot(),
		PagesModule,
        BrowserAnimationsModule,
        NoopAnimationsModule,
        HeaderInfoModule,
        CatalogModule,
		ContactsModule,
        FaqModule,
        BusketModule,
		UnSnackBarModule,
		MenuModule,
		SocialBarModule
	],
	providers: [
		BusketService,
		UnSnackBarService,
        {provide: MAT_DATE_LOCALE, useValue: 'ru-RU'},
        // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
        // `MatMomentDateModule` in your applications root module. We provide it at the component level
        // here, due to limitations of our example generation script.
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
		GoogleAnalyticsEventsService
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
