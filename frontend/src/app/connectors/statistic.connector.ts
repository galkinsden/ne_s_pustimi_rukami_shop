import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {appConfig} from '../app.config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {ConnectorErrorModel, IError} from "../modules/connector-error.model";
import {IMailSendRequest} from "../modules/mail.service";
import {ISocialFullInfo} from "../pages/catalog/models/social-full-Info.model";
import {IStatisticInfo} from "../pages/catalog/models/statistic-info.model";

export interface IStatisticConnectorGetResponse {
    data: IStatisticInfo;
    error: ConnectorErrorModel;
}
export class StatisticConnector {

    constructor(private http: HttpClient) {}

    public getStatistic(): Observable<IStatisticInfo | ConnectorErrorModel> {
        return this.http.get(`${appConfig.apiUrl}/statisticInfo-request`)
            .map((res: IStatisticConnectorGetResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }


    private handleError(res: IError): Observable<ConnectorErrorModel> {
        return Observable.throw(new ConnectorErrorModel(res));
    }
}