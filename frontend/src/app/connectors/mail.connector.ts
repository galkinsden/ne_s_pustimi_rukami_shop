import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {appConfig} from '../app.config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {ConnectorErrorModel, IError} from "../modules/connector-error.model";
import {IMailSendRequest} from "../modules/mail.service";

export interface IMailConnectorSendMailResponse {
    data: boolean;
    error: ConnectorErrorModel;
}

export class MailConnector {

    constructor(private http: HttpClient) {}

    public sendMail(data: IMailSendRequest): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/mail-request`, {data: data})
            .map((res: IMailConnectorSendMailResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    private handleError(res: IError): Observable<ConnectorErrorModel> {
        return Observable.throw(new ConnectorErrorModel(res));
    }
}