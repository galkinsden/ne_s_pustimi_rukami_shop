import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {appConfig} from '../app.config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {ICatalog} from "../pages/catalog/models/catalog.model";
import {ConnectorErrorModel, IError} from "../modules/connector-error.model";
import {IAttachmentModel, IGiftWithAttachments} from "../pages/catalog/models/gift-with-attachments.model";
import {IGift} from "../pages/catalog/models/gift.model";

export interface ICatalogConnectorGetAllResponse {
    data: ICatalog;
    error: IError;
}

export interface ICatalogConnectorGetAttachmentsByIdResponse {
    data: IAttachmentModel;
    error: IError;
}

export interface ICatalogConnectorGetCatalogWithAttachmentsByIdResponse {
    data: IGiftWithAttachments;
    error: IError;
}

export interface ICatalogConnectorGetGiftByIdResponse {
    data: IGift;
    error: IError;
}

export interface ICatalogConnectorGetGiftsByIdsResponse {
    data: IGift[];
    error: IError;
}

export class CatalogConnector {

    constructor(private http: HttpClient) {}

    public getAll(): Observable<ICatalog | ConnectorErrorModel> {
        return this.http.get(`${appConfig.apiUrl}/catalog-request`)
            .map((res: ICatalogConnectorGetAllResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getCatalogWithAttachmentsById(id: string): Observable<IGiftWithAttachments | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/catalog-with-attachments`, {id: id})
            .map((res: ICatalogConnectorGetCatalogWithAttachmentsByIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getGiftById(id: string): Observable<IGift | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/catalog-request/id`, {id: id})
            .map((res: ICatalogConnectorGetGiftByIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getGiftsByIds(ids: string[]): Observable<IGift[] | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/catalog-request/ids`, {ids: ids})
            .map((res: ICatalogConnectorGetGiftsByIdsResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getAttachmentsById(id: string): Observable<IAttachmentModel | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/attachments-request`, {id: id})
            .map((res: ICatalogConnectorGetAttachmentsByIdResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    private handleError(res: IError): Observable<ConnectorErrorModel> {
        return Observable.throw(new ConnectorErrorModel(res));
    }
}