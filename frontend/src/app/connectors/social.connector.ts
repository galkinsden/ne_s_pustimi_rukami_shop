import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {appConfig} from '../app.config';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import {ConnectorErrorModel, IError} from "../modules/connector-error.model";
import {IMailSendRequest} from "../modules/mail.service";
import {ISocialFullInfo} from "../pages/catalog/models/social-full-Info.model";

export interface ISocialConnectorSendMailPostResponse {
    data: boolean;
    error: ConnectorErrorModel;
}
export interface ISocialConnectorSendMailGetResponse {
    data: ISocialFullInfo[];
    error: ConnectorErrorModel;
}

export class SocialConnector {

    constructor(private http: HttpClient) {}

    public getAllInfo(): Observable<ISocialFullInfo[] | ConnectorErrorModel> {
        return this.http.get(`${appConfig.apiUrl}/social-request`)
            .map((res: ISocialConnectorSendMailGetResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getViewById(id: string): Observable<ISocialFullInfo[] | ConnectorErrorModel> {
        return this.http.get(`${appConfig.apiUrl}/social-request/view/${id}`, )
            .map((res: ISocialConnectorSendMailGetResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getLikesById(id: string): Observable<ISocialFullInfo[] | ConnectorErrorModel> {
        return this.http.get(`${appConfig.apiUrl}/social-request/likes/${id}`, )
            .map((res: ISocialConnectorSendMailGetResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getPurchasesById(id: string): Observable<ISocialFullInfo[] | ConnectorErrorModel> {
        return this.http.get(`${appConfig.apiUrl}/social-request/purchases/${id}`, )
            .map((res: ISocialConnectorSendMailGetResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public getCommentsById(id: string): Observable<ISocialFullInfo[] | ConnectorErrorModel> {
        return this.http.get(`${appConfig.apiUrl}/social-request/comment/${id}`, )
            .map((res: ISocialConnectorSendMailGetResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public setCommentById(id: string, text: string): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/social-request/comment`, {id: id, text: text})
            .map((res: ISocialConnectorSendMailPostResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public setPurchasesById(id: string, count: number): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/social-request/purchases`, {id: id, count: count || 1})
            .map((res: ISocialConnectorSendMailPostResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public setLikeById(id: string): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/social-request/likes`, {id: id})
            .map((res: ISocialConnectorSendMailPostResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }

    public setViewById(id: string): Observable<boolean | ConnectorErrorModel> {
        return this.http.post(`${appConfig.apiUrl}/social-request/view`, {id: id})
            .map((res: ISocialConnectorSendMailPostResponse) => res.data)
            .catch((res: HttpErrorResponse) => this.handleError(res.error && res.error.error ? res.error.error : res.error));
    }


    private handleError(res: IError): Observable<ConnectorErrorModel> {
        return Observable.throw(new ConnectorErrorModel(res));
    }
}